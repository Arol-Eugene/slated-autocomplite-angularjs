export default class requestService {
  constructor($http, $q) {
    'ngInject';

    this._$http = $http;
    this._$q = $q;

    this.apiUrl = 'http://www.slated.com/films/autocomplete/profiles/';
    this.canceler = null;
  }

  getFilms(term) {
    if (!term.length) { return this._$q.resolve([])}
    const url = `${this.apiUrl}?term=${term}`;

    if (this.canceler) {
      this.canceler.resolve();
    }
    this.canceler = this._$q.defer();

    return this._$http.get(url, { timeout: this.canceler.promise })
      .then((res) => res.data);
  }
}

