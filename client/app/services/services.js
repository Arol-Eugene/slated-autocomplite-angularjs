import angular from 'angular';

import requestService from './request.service';

let serviceModule = angular.module('app.services', [])
  .service({ requestService })

.name;

export default serviceModule;
