import angular from 'angular';
import blockModule from './blocks/blocks';

let componentModule = angular.module('app.components', [
  blockModule,
])

.name;

export default componentModule;
