import template from './b-autocomplete.html';
import controller from './b-autocomplete.controller.js';
import './b-autocomplete.styl';

let BAutocompleteComponent = {
  template,
  controller,
  bindings: {
    placeholder: '@',
    data: '=',
    getData: '&',
  },
};

export default BAutocompleteComponent;
