class BAutocompleteController {
  constructor() {
    'ngInject';

    this.model = '';
    this.isVisible = false;
    this.isLoading = false;
  }

  onChange() {
    this.isLoading = true;
    this.handleDataLoading();
  }

  handleDataLoading(model) {
    this.getData({ model: this.model })
      .then(() => (this.isLoading = false))
      .catch((err) => {
        if (err.xhrStatus !== 'abort') {
          this.isLoading = false;
        }
      })
  }

  selectItem(item) {
    this.model = item.value;
    this.isVisible = false;
  }

  resetData() {
    this.model = '';
    this.handleDataLoading();
  }
}

export default BAutocompleteController;
