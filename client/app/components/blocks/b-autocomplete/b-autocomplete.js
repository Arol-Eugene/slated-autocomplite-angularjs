import angular from 'angular';
import bAutocompleteComponent from './b-autocomplete.component';

let bAutocompleteModule = angular.module('b-autocomplete', [])

  .component('bAutocomplete', bAutocompleteComponent)

.name;

export default bAutocompleteModule;
