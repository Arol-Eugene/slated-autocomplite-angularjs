import angular from 'angular';

import bAutocompleteModule from './b-autocomplete/b-autocomplete';

let blockModule = angular.module('app.blocks', [
  bAutocompleteModule,
])

.name;

export default blockModule;


