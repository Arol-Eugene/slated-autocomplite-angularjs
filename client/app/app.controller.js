class AppController {
  constructor(requestService) {
    'ngInject';

    this._requestService = requestService;

    this.films = [];
  }

  getFilms({ model }) {
    return this._requestService.getFilms(model)
      .then((films) => (this.films = films));
  }
}

export default AppController;
