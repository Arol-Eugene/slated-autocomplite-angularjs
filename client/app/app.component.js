import template from './app.html';
import controller from './app.controller.js';
import './app.styl';

let appComponent = {
  template,
  controller,
  restrict: 'E',
};

export default appComponent;
