export default function ($sce) {
  'ngInject';

  return (html) => $sce.trustAsHtml(html);
}
