import angular from 'angular';

import unsafe from './unsafe.filter';


let filtersModule = angular.module('app.filters', [])
  .filter({ unsafe })

.name;

export default filtersModule;
