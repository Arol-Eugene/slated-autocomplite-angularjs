import angular from 'angular';
import toggleDropdownModule from './toggle-dropdown/toggle-dropdown';

let sharedModule = angular.module('app.shared', [
  toggleDropdownModule,
])

.name;

export default sharedModule;
