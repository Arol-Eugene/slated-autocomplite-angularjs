export default function ($window) {
  'ngInject';

  return {
    scope: {
      isVisible: '=',
    },
    link: ($scope, $element) => {
      const toggleDropdown = (event) => {
        const isInElem = $element[0].contains(event.target);

        if (!isInElem && $scope.isVisible) {
          $scope.isVisible = false;
          $scope.$apply();
        } else if (isInElem && !$scope.isVisible) {
          $scope.isVisible = true;
          $scope.$apply();
        }
      };

      angular.element($window).on('click', toggleDropdown);

      $scope.$on('$destroy', () => {
        angular.element($window).off('click', toggleDropdown);
      });
    }
  }
}
