import angular from 'angular';
import toggleDropdown from './toggle-dropdown.directive';

let toggleDropdownModule = angular.module('toggle-dropdown', [])

  .directive({ toggleDropdown })

  .name;

export default toggleDropdownModule;
