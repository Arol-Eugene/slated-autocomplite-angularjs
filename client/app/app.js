import angular from 'angular';

import Components from './components/components';
import Services from './services/services';
import Shared from './shared/shared';
import Filters from './filters/filters';

import AppComponent from './app.component';

angular.module('app', [
  Components,
  Services,
  Shared,
  Filters,
])
  .component('app', AppComponent);
